﻿#ifndef PCH_H
#define PCH_H

#include <iostream> 
#include <fstream> /* для работы с файлами */
#include <sstream> /* для работы с std::stringstream */
#include <iomanip> /* для функции setw() */
#include <string>
#include <set>

// Название файла который хранит записи о проданих мячах
#define FILE_NAME "Balls.dat"
// Название файла который хранит записи о проданих мячах
#define FILE_TMP "Balls.tmp"
// Максимальная длина имени мяча
#define MAX_MARKA_SIZE 11

// структура записи о продаже мячей
struct Balls
{
	int type =0,				  // тип мяча
		material = 0,             // материал
		country = 0,              // страна производитель
		count = 0;                // количество мячей
	float price =0.0f,             // цена за мяч
		all_price = 0.0f;            // цена за все мячи
	char marka[MAX_MARKA_SIZE]; // название маяча
};

// типы мячей
enum BallTypes
{
	FOOTBALL = 1, // футбольный 
	VOLLEYBALL,   // волейбольный
	BASKETBALL    // баскетбольный
};

// материалы мячей
enum BallMaterials
{
	SYNTHETIC = 1, // Синтетична шкiра
	ARTIFICIAL,    // Штучна шкiра
	COMPOSITE,     // Композитна шкiра
	NATURAL        // Натуральна шкiра
};

// материалы мячей
enum BallCountrys
{
	UKRAINE = 1, // Украина
	USA,         // США
	CHINA,       // Китай 
	JAPAN,       // Япония
	DENMARK,     // Дания 
	SPAIN,       // Испания
};

#endif //PCH_H